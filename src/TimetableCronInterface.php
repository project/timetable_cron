<?php

namespace Drupal\timetable_cron;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a timetablecron entity type.
 */
interface TimetableCronInterface extends ConfigEntityInterface {

}
